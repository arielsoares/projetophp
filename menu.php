<?php

echo "
<nav class='navbar navbar-expand-lg navbar-dark' style='background-color: #562A72'>
  <a class='navbar-brand' href='?pg=principal'>CN</a>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarColor02' aria-controls='navbarColor02' aria-expanded='false' aria-label='Toggle navigation'>
    <span class='navbar-toggler-icon'></span>
  </button>

  <div class='collapse navbar-collapse' id='navbarColor02'>
    <ul class='navbar-nav mr-auto'>
      <li class='nav-item active'>
        <a class='nav-link' href='?pg=calculadora'>Calculadora<span class='sr-only'>(current)</span></a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='?pg=quemsomos'>Quem Somos</a>
      </li>
      <li class='nav-item'>
        <a class='nav-link' href='?pg=contato'>Fale conosco!</a>
      </li>
    </ul>
  </div>
</nav>";