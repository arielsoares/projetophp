-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Out-2018 às 04:47
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minisite`
--
CREATE DATABASE IF NOT EXISTS `minisite` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `minisite`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dadosdepaciente`
--

CREATE TABLE `dadosdepaciente` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `idade` int(3) NOT NULL,
  `sexo` int(10) NOT NULL,
  `peso` float NOT NULL,
  `altura` float NOT NULL,
  `ati` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `dadosdepaciente`
--

INSERT INTO `dadosdepaciente` (`id`, `nome`, `idade`, `sexo`, `peso`, `altura`, `ati`) VALUES
(5, 'Ariel elel', 25, 0, 71.3, 1.8, 5),
(6, 'Ariel', 29, 0, 71.3, 1.8, 2),
(7, 'gabriella', 25, 0, 75, 1.65, 6),
(8, 'gabriella', 25, 0, 75, 1.65, 6),
(9, 'Ariel', 25, 0, 71.3, 1, 2),
(10, 'Ariel', 25, 0, 71.3, 1, 2),
(11, 'Ariel', 25, 0, 71.3, 1.8, 2),
(12, 'gabriella', 22, 1, 75, 1.65, 7),
(13, 'gabriella', 22, 1, 75, 1.65, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `faleconosco`
--

CREATE TABLE `faleconosco` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `assunto` varchar(100) COLLATE utf8_bin NOT NULL,
  `mensagem` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `quemsomos`
--

CREATE TABLE `quemsomos` (
  `id` int(11) NOT NULL,
  `texto` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `quemsomos`
--

INSERT INTO `quemsomos` (`id`, `texto`) VALUES
(1, '    <div><center><h3>Calculadora de Nutrição</h3></center></div>\r\n    \r\n    <div><p>\r\n        Lorem ipsum ultrices odio conubia posuere pellentesque hac erat, interdum tincidunt dapibus fermentum nullam vulputate laoreet nisi gravida, donec faucibus nisi porta est praesent felis. nibh aptent curabitur aptent praesent convallis id odio consectetur amet, dolor dapibus fringilla vitae suspendisse vitae cras erat, donec lorem conubia libero posuere curabitur eget ultrices. etiam molestie libero aenean diam gravida suspendisse quis, nisi imperdiet ut consequat sagittis pulvinar tellus, accumsan dui interdum donec tincidunt at. nunc etiam dui risus egestas fames gravida eu condimentum suscipit tempor class nec justo porttitor, condimentum non augue dictum aliquet habitant posuere per et phasellus adipiscing condimentum elementum. </p>\r\n\r\n	<p>In fermentum id at nullam porta sodales augue, egestas tempus lacus libero euismod eu pretium aptent, netus quam ut proin condimentum non. velit lobortis pulvinar non aliquet facilisis donec morbi hac, est per inceptos amet sed netus ligula, interdum lectus felis donec ut eget mauris. orci taciti neque tempus conubia purus aptent dapibus per massa, tincidunt sociosqu id malesuada taciti purus urna iaculis enim, commodo vitae aenean nullam dictum posuere in pharetra. eleifend commodo accumsan litora duis nisi commodo quisque eu, cursus tincidunt enim maecenas consequat habitasse vel diam ipsum, iaculis odio malesuada volutpat fusce ipsum volutpat. \r\n</p>\r\n	<p>Tellus nisl porta torquent conubia tristique nisl blandit suspendisse convallis sodales, lectus aenean aliquam potenti massa himenaeos quam blandit lobortis, elementum mollis eros quisque iaculis tempus aliquam est interdum convallis, donec id netus viverra cubilia scelerisque blandit suscipit. vitae curabitur orci ut commodo adipiscing aptent pulvinar, auctor fermentum fringilla tempor pharetra faucibus aliquam libero, tortor ut ullamcorper habitant massa metus. viverra non rhoncus ut dui felis neque, turpis phasellus neque at diam nibh, netus morbi inceptos pulvinar rutrum. mollis leo blandit sem cursus morbi molestie, etiam congue metus curae quisque bibendum, vulputate volutpat primis dui curae. \r\n\r\n	Curabitur quam tristique laoreet ultrices, eget vel. \r\n        </p>\r\n    </div>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dadosdepaciente`
--
ALTER TABLE `dadosdepaciente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faleconosco`
--
ALTER TABLE `faleconosco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quemsomos`
--
ALTER TABLE `quemsomos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dadosdepaciente`
--
ALTER TABLE `dadosdepaciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `faleconosco`
--
ALTER TABLE `faleconosco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quemsomos`
--
ALTER TABLE `quemsomos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
