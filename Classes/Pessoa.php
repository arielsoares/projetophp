<?php

/**
 * Description of Pessoa
 *
 * @author Ariel
 */
class Pessoa {
    
    private $nome;
    private $idade;
    private $sexo;
    private $peso;
    private $altura;
    private $ati; //dias da semana de atividade física
    
    
    function __construct($nome, $idade, $sexo, $peso, $altura, $ati) {
        $this->nome = $nome;
        $this->idade = $idade;
        $this->sexo = $sexo;
        $this->peso = $peso;
        $this->altura = $altura;
        $this->ati = $ati;
    }
    
    function getNome() {
        return $this->nome;
    }

    function getIdade() {
        return $this->idade;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getPeso() {
        return $this->peso;
    }

    function getAltura() {
        return $this->altura;
    }

    function getAti() {
        return $this->ati;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setPeso($peso) {
        $this->peso = $peso;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function setAti($ati) {
        $this->ati = $ati;
    }




}
