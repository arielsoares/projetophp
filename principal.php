<?php

echo "<div class='container'>
    <h3 class='text-center'>Instruções de preenchimento</h3>
    <p>Antes de começar o preenchimento do formulário, atente-se as informações que serão solicitadas sobre o paciente:</p>
    <ul><li>Nome</li>
     <li>Idade</li>
    <li>Sexo</li>
    <li>Peso (kg) – Para se obter um peso acurado, utilizar
    roupas leves. Usar uma balança
    calibrada e confiável.</li>
    <li>Altura (cm) – Medir a altura sem sapatos, utilizando
    um estadiômetro (equipamento para medir altura)
    e, se o paciente estiver acamado, pela altura do joelho
    ao calcanhar, ou semienvergadura.</li>
    <li> Dias que pratica atividade física.</li></ul>
    </p>";


?>